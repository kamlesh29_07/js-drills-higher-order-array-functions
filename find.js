// function find(elements, cb) {
//     // Do NOT use .includes, to complete this function.
//     // Look through each value in `elements` and pass each element to `cb`.
//     // If `cb` returns `true` then return that element.
//     // Return `undefined` if no elements pass the truth test.
// }

export function find(elements,cb){
    let  count = 0;
    for(let i = 0;i<elements.length;i++){
        const result = cb(elements[i]);
        if(result == true){
            console.log(elements[i]);
            count += 1;
        }
    }
    if(count == 0){
        console.log("undefined");
    }
}
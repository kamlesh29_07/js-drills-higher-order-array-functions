//function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
//}
export function filter(elements,cb){
    const array = [];
    for(let i = 0;i<elements.length;i++){
        const result = cb(elements[i]);
        if(result == true){
            array.push(elements[i]);
        }
    }
    console.log(array);
}

  
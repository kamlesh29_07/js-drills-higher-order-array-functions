//function each(elements, cb) {
    // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
    // You should also pass the index into `cb` as the second argument
    // based off http://underscorejs.org/#each
//}

export function each(items,cb) {
    for(let i = 0;i<items.length;i++){
        cb(items[i],i);
    }
}

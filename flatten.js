// const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

// function flatten(elements) {
//     // Flattens a nested array (the nesting can be to any depth).
//     // Hint: You can solve this using recursion.
//     // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
// }

export function flatten(nestedArray){
    let  flatArray = [];
    for(let i = 0;i<nestedArray.length;i++){
        if(Array.isArray(nestedArray[i])){
            flatArray = flatArray.concat(flatten(nestedArray[i]));
        }
        else{
            flatArray.push(nestedArray[i]);
        }
    }
    return flatArray;
}